# Appidon

Appidon, est une application mobile qui permet aux utilisateurs d’effectuer des dons aux associations de leur choix en jouant à des jeux mobile.

Lorsque l'utilisateur se met à jouer, le smartphone génère du CPU qui va miner une cryptomonnaie, le cryptonight. Cette monnaie virtuelle identifiée sous forme de blocs de hash, est ensuite récupérée sur notre plateforme et redistribuée aux associations choisies par l'utilisateur.

Appidon est une solution unique qui défie toutes concurrences. En effet, l'utilisateur va pouvoir contribuer caritativement de façon ludique, automatique et rapide. C'est donc la solution parfaite pour notre principale cible que sont les jeunes.
